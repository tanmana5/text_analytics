# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* To scrape information of interest from journalistic websites and run some text analytics on them.

The input text file consists mainly of text paragraphs from featured articles  on Yale E360 newsletter on the yale360.edu website.
This is intended for learning/exploration purpose only and is not intended
for any commercial use of the material.

### How do I get set up? ###

* Set up on Python 2 environment
* Run the web_scraping.py script to get access to the text articles and download all test into a csv file.
* Run the topic_detection.py script to get a list of topics.

### Who do I talk to? ###
* tanmana5@gmail.com
