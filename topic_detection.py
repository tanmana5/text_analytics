"""This script performs topic (keywords) detection on input text file.

DISCLAIMER: The input text file consists mainly of text paragraphs
from featured articles  on Yale E360 newsletter on the yale360.edu website.
This is intended for learning/exploration purpose only and is not intended
for any commercial use of the material.

Some text portions (obtained by running script web_scraping.py) have been
cleaned to keep relevant text. Please note, further cleaning is necessary to
generate more meaningful results.

Parameters (should be exposed as args):
csvfile - csv file containing textual information from which topics generated.
ratio_range - range of values for gensim method which keywords to be extracted.

Outputs:
keywords - gensim extracted keywords for corresponding(supplied) ratio.
"""
from os import path as osp
import csv
from gensim.summarization.summarizer import summarize
from gensim.summarization import keywords


filepath = '/home/tammy/text_analytics/texts'
filename = 'yale_360_texts.csv'
csvfile = osp.join(filepath, filename)

with open(csvfile, 'r') as csvfile:
    file = csv.reader(csvfile, delimiter=',')
    for row in file:
        text = ', '.join(row)

print('\nKeywords for different ratios:')
# higher ratio => more keywords
ratio_range = (x/10 for x in range(1, 4))
for ratio in ratio_range:
    print('\nRATIO=', ratio, 'Keywords:')
    print(keywords(str(text), ratio=ratio))
