"""LSTM to compare with HMM for classifying poem sequences by poets."""

import string
import numpy
import numpy as np
import matplotlib.pyplot as plt
from sklearn.utils import shuffle
from nltk import pos_tag, word_tokenize
import pandas as pd

from keras.datasets import imdb
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
from keras.layers.embeddings import Embedding
from keras.preprocessing import sequence
numpy.random.seed(7)
MAX_REVIEW_LENGTH = 250
N = 500   # number of records to test
# max no of words to consider in vocab according to freq of occurrence
top_words = 5000


def get_tags(s):
    tuples = pos_tag(word_tokenize(s))
    return [y for x, y in tuples]


def get_data(files):
    word2idx = {}
    current_idx = 0
    X = []
    Y = []

    for fn, label in zip(files[0], files[1], (0, 1)):
        count = 0
        for line in open(fn):
            line = line.rstrip()
            if line:
                print(line)
                # tokens = remove_punctuation(line.lower()).split()
                tokens = get_tags(line)
                if len(tokens) > 1:
                    # scan doesn't work nice here, technically could fix...
                    for token in tokens:
                        if token not in word2idx:
                            word2idx[token] = current_idx
                            current_idx += 1
                    sequence = np.array([word2idx[w] for w in tokens])
                    X.append(sequence)
                    Y.append(label)
                    count += 1
                    print(count)
                    if count >= 1000:
                        break
    print("Vocabulary:", word2idx.keys())
    return X, Y, current_idx


def truncate_lengths(X_train, X_test, max_review_length=MAX_REVIEW_LENGTH):
    # truncate and pad input sequences
    X_train_trunc = sequence.pad_sequences(X_train, maxlen=max_review_length)
    X_test_trunc = sequence.pad_sequences(X_test, maxlen=max_review_length)
    return X_train_trunc, X_test_trunc


def main():
    # get the top_words only from data set (left)
    # TODO: convert file inputs to args
    files = ('robert_frost.txt', 'edgar_allan_poe.txt')
    X, Y, V = get_data(files)
    print("len(X):", len(X))
    print("Vocabulary size:", V)
    X, Y = shuffle(X, Y)

    X_train, Y_train = X[:-N], Y[:-N]
    X_test, Y_test = X[-N:], Y[-N:]

    seed = 100
    np.random.seed(seed)
    np.random.shuffle(Y_train)
    np.random.seed(seed)
    np.random.shuffle(Y_train)

    np.random.seed(seed * 2)
    np.random.shuffle(Y_test)
    np.random.seed(seed * 2)
    np.random.shuffle(Y_test)

    # truncate and pad input sequences
    X_train_trunc, X_test_trunc = truncate_lengths(X_train, X_test)
    X_train_2d = np.vstack(X_train_trunc)
    X_test_2d = np.vstack(X_test_trunc)

    print('Sample records:')
    print(X_train[0:20])
    print ("\ntransformed records:\n")
    print(X_train_2d[0:20])

    # create the model
    # TODO: Create model config file
    embedding_vector_length = 32
    model = Sequential()
    model.add(Embedding(top_words, embedding_vector_length,
              input_length=MAX_REVIEW_LENGTH))
    model.add(LSTM(100))
    model.add(Dense(1, activation='sigmoid'))
    model.compile(loss='binary_crossentropy', optimizer='adam',
                  metrics=['accuracy'])
    print(model.summary())
    model.fit(X_train_2d, Y_train, validation_data=(X_test_2d, Y_test),
              epochs=10, batch_size=64)
    yFit = model.predict(X_test_2d)
    print(yFit)


if __name__ == '__main__':
    main()
