"""Created on Sun Aug 20 18:39:00 2017.

This code is to obtain headings and information from the
online newsletter at http://e360.yale.edu/
@author: Tammy
"""
# imports
from __future__ import print_function

import time
from os import path as osp
import numpy as np
import csv
from bs4 import BeautifulSoup
import requests
import pandas as pd
import pprint


class ScrapeWebPage:
    def __init__(self, base_location):
        self.base_location = base_location

    def extract_article_text(self, link):
        req = requests.get(link, verify=False)
        soup = BeautifulSoup(req.content, 'lxml')
        texts = soup.text
        return texts

    def get_texts(self, soup):
        article_titles = soup.find_all('h2')
        texts = []
        print('Extracting text from all links:')
        for article in article_titles:
            link = ''.join(article.find_all('a', href=True)[0]['href'])
            texts.append(self.extract_article_text(link))
        return texts

    def save_to_file(self, texts, filepath, filename):
        csvfile = osp.join(filepath, filename)
        with open(csvfile, 'w') as output:
            writer = csv.writer(output, lineterminator='\n')
            for text in texts:
                writer.writerow([text])


def main():
    base_location = 'https://e360.yale.edu/features/'
    # output file path and name to store extrcated texts
    filepath = '/home/tammy/text_analytics/texts'
    filename = 'yale_360_texts.csv'
    r = requests.get(base_location, verify=False)
    soup = BeautifulSoup(r.content, 'lxml')
    scraper = ScrapeWebPage(base_location)
    texts = scraper.get_texts(soup)
    scraper.save_to_file(texts, filepath, filename)


if __name__ == '__main__':
    main()
